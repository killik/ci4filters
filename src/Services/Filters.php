<?php

namespace Killik\CI4Filters\Services;

use CodeIgniter\Filters\Exceptions\FilterException;
use CodeIgniter\Filters\Filters as FiltersFilters;

class Filters extends FiltersFilters
{
    /**
     * Ensures that a specific filter is on and enabled for the current request.
     *
     * Filters can have "arguments". This is done by placing a colon immediately
     * after the filter name, followed by a comma-separated list of arguments that
     * are passed to the filter when executed.
     *
     * @param string|array $filter
     * @param string $when
     *
     * @return \CodeIgniter\Filters\Filters
     */
    public function enableFilter($filter, string $when = 'before')
    {
        if (!is_array($filter)) {

            $name = $filter;

            // Get parameters and clean name
            if (strpos($name, ':') !== false) {
                list($name, $params) = explode(':', $name);

                $params = explode(',', $params);
                array_walk($params, function (&$item) {
                    $item = trim($item);
                });

                $this->arguments[$name] = $params;
            }

            if (!array_key_exists($name, $this->config->aliases)) {
                throw FilterException::forNoAlias($name);
            }

            if (!isset($this->filters[$when][$name])) {
                $this->filters[$when][] = $name;
            }

            return $this;
        }

        foreach ($filter as $item) $this->enableFilter($item, $when);

        return $this;
    }
}
