<?php

namespace Killik\CI4Filters;

use CodeIgniter\Config\Services as ConfigServices;
use Killik\CI4Filters\Services\Filters;
use Killik\CI4Filters\Services\RouteCollection;

class Services extends ConfigServices
{
    public static function routes(bool $getShared = true): RouteCollection
    {
        if ($getShared) {
            return static::getSharedInstance('routes');
        }

        return new RouteCollection(static::locator(), config('Modules'));
    }

    public static function filters($config = null, bool $getShared = true): Filters
    {
        if ($getShared) {
            return static::getSharedInstance('filters', $config);
        }

        if (empty($config)) {

            $config = new \Config\Filters();
        }

        return new Filters($config, static::request(), static::response());
    }
}
